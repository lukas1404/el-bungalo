$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel({
        interval: 1000
    })
    $('#user-data').on('show.bs.modal', function(e) {
        console.log('el modal de contacto se esta mostrando');

        $('#bt-modal').removeClass('btn-primary');
        $('#bt-modal').addClass('btn-outline-success');
        $('#bt-modal').prop('disabled', true);
    });
    $('#user-data').on('shown.bs.modal', function(e) {
        console.log('el modal de contacto se mostro');
    });
    $('#user-data').on('hide.bs.modal', function(e) {
        console.log('el modal de contacto se esta ocultando');
    });
    $('#user-data').on('hidden.bs.modal', function(e) {
        console.log('el modal de contacto se oculto');

        $('#bt-modal').removeClass('btn-outline-success');
        $('#bt-modal').addClass('btn-primary');
        $('#bt-modal').prop('disabled', false);
    });


});